# ChatonsInfos

**ChatonsInfos** est un protocole de partage de données sur le collectif, ses membres et leurs services.

L’idée est de faire ça de façon simple et légère via des fichiers de description accessibles par lien web : 1 fichier par chaton et 1 fichier par service.

Maintenus en autonomie par les chatons volontaires, ces fichiers seront collectés régulièrement et valorisés dans le futur site https://stats.chatons.org/.

 En favorisant un partage d’information sur les chatons et leurs services, les résultats attendus sont :

* une meilleure visibilité des chatons et de leurs services ;
* une mesure de l’activité des services : même incomplète ou partielle, cela donnera idée de l’activité globale.

## LICENCE

Sauf mention contraire, les documents du projet ChatonsInfos sont diffusés sous la licence libre Creative Commons CC-BY-SA.


## AUTEUR

Le collectif CHATONS.

## DOCUMENTATION

Plan de la documentation :
* README.md : accueil ;
* CONCEPTS.md : les concepts fondamentaux ;
* FAQ.md : une foire aux questions ;
* MODELES/federation.properties : modèle de fihcier _properties_ pour le collectif CHATONS ;
* MODELES/organization.properties : modèle de fichier _properties_ pour un membre du collectif CHATONS ;
* MODELES/services.properties : modèle de fichier _properties_ générique pour un service d'un membre du collectif CHATONS ;
* MODELES/services-xxxxx.properties : modèle de fichier _properties_ d'un logiciel précis, utile pour les métriques spécifiques.
* StatoolInfos/ : dossier de configuration de l'outil StatoolInfos, le générateur des pages de https://stats.chatons.org/.

## LOGO

À propos du logo :
* fichier : logo_ChatonsInfos.jpg ;
* source : projet StatoolInfos ;
* auteur: Christian Pierre MOMON christian.momon@devinsy.fr
* licence: Creative Commons CC-BY-SA last version.
